import { Component, OnInit } from '@angular/core';
import { ServicioService } from '../app/services/servicio.service';
import {User} from '../app/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  User: User;
  
  ngOnInit(): void {
    
  }

  constructor(private locationService:ServicioService){
   
  }


  
  

postallLocation(){
    this.locationService.send_post(this.User).subscribe();
    console.log(this.User);
}


  title = 'frontlocation';
}
