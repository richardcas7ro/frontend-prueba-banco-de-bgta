import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {

  url = environment.url;

  constructor(private http: HttpClient) { }

  send_post(information){
    return this.http.post(this.url,information);
  }

  get_locations(){
    return this.http.get(this.url);
  }
}
